FROM php:7.3-apache

RUN apt-get update -y && apt-get install -y sendmail libpng-dev libicu-dev

RUN docker-php-ext-install mysqli  gd intl 

RUN apt-get install -y libc-client-dev libkrb5-dev && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install imap